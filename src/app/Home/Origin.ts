export interface Origin {
    id: number;
    name: string;
    airlines: string;
    arrival: Date;
    carrier: string;
    departDate: Date;
    destination: string;
    distance: number
    fNumber: string;
    departsFrom: string;
    status: string;
    totalTime: string;
  }