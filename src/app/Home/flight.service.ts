import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Origin } from './Origin';

@Injectable({
  providedIn: 'root'
})
export class FlightService {

  constructor(private httpService: HttpClient) { }

  searchFlights(from: any, to: any, newVal: any): Observable<Origin[]> {
    return this.httpService.get<Origin[]>('/home/search/' + from + '/' + to + '/' + newVal);
  }

  searchFromNumber(fnumber: any, departDate: any): Observable<Origin[]> {
    return this.httpService.get<Origin[]>('/home/search/number/' + fnumber + '/' + departDate);
  }
}
