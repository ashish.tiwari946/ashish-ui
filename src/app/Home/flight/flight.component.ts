import { Component, OnInit } from '@angular/core';
import { FlightService } from '../flight.service';
import { Origin } from '../Origin';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-flight',
  templateUrl: './flight.component.html',
  styleUrls: ['./flight.component.css']
})
export class FlightComponent implements OnInit {

  search: any;
  fsearchList: Origin[] = [];
  respSuccess: boolean;
  noResponse: string;

  constructor(private service: FlightService, private toastr: ToastrService) {
    this.search = {
      opt: true
    };
  }

  ngOnInit() {
  }

  searchFlights() {
    this.respSuccess = false;
    this.fsearchList;
    if (this.search.departsFrom == null || this.search.departsFrom == "" || this.search.departsFrom == undefined || this.search.destination == "" || this.search.destination == null || this.search.destination == undefined || this.search.departDate == null || this.search.departDate == "" || this.search.departDate == undefined) {
      this.toastr.error("Not allowed", 'Fields are required!', {
        timeOut: 3000
      });
    } else {
      let jsonDate = (this.search.departDate).toJSON();
      this.service.searchFlights(this.search.departsFrom, this.search.destination, jsonDate)
        .subscribe((response: Origin[]) => {
          this.fsearchList = response;
          if (this.fsearchList.length > 0) {
            this.respSuccess = true;
          } else {
            this.respSuccess = false;
            this.noResponse = "No value found realted to search criteria!"
          }
          console.log(response);
        })
    }
  }
  searchFromNumber() {
    this.respSuccess = false;
    this.fsearchList;
    if (this.search.fdepartDate == null || this.search.fdepartDate == "" || this.search.fdepartDate == undefined || this.search.fNumber == "" || this.search.fNumber == null || this.search.fNumber == undefined) {
      this.toastr.error("Not allowed", 'Fields are required!', {
        timeOut: 3000
      });
    } else {
      let jsonDate = (this.search.fdepartDate).toJSON();
      this.service.searchFromNumber(this.search.fNumber, jsonDate)
        .subscribe((response: Origin[]) => {
          this.fsearchList = response;
          if (this.fsearchList.length > 0) {
            this.respSuccess = true;
          } else {
            this.respSuccess = false;
            this.noResponse = "No value found realted to search criteria!"
          }
          console.log(response);
        })
    }
  }
}